import { ApiRequest, DefaultGitLabClient } from '@gitlab/gitlab-api-client';

type ApiRequestCommand<T> = (request: ApiRequest<T>) => Promise<T>;

export const commandFactory =
  <T>(client: DefaultGitLabClient): ApiRequestCommand<T> =>
  async request =>
    client.fetchFromApi(request);
