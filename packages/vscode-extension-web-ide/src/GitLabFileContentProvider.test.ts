import { GitLabFileContentProvider } from './GitLabFileContentProvider';
import { fetchFileRaw } from './mediator';

jest.mock('./mediator');

const TEST_REF = '111111000000';
const TEST_PATH = '/foo/README.md';

const TEST_FETCH_RESPONSE = {
  buffer: Buffer.from('Hello world!'),
};

describe('GitLabFileContentProvider', () => {
  let subject: GitLabFileContentProvider;

  beforeEach(() => {
    subject = new GitLabFileContentProvider(TEST_REF);

    jest.mocked(fetchFileRaw).mockResolvedValue(TEST_FETCH_RESPONSE);
  });

  it('rate limits getContent', async () => {
    jest.useRealTimers();

    const startTime = performance.now();
    await Promise.all(
      Array(30)
        .fill(1)
        .map(async () => {
          await subject.getContent(TEST_PATH);
        }),
    );
    const firstBatchTime = performance.now() - startTime;

    expect(fetchFileRaw).toHaveBeenCalledTimes(30);
    expect(firstBatchTime).toBeLessThan(1000);

    await Promise.all(
      Array(30)
        .fill(1)
        .map(async () => {
          await subject.getContent(TEST_PATH);
        }),
    );
    const secondBatchTime = performance.now() - startTime;

    expect(fetchFileRaw).toHaveBeenCalledTimes(60);
    expect(secondBatchTime).toBeGreaterThan(6000);
  }, 10000);

  describe('getContent', () => {
    let result: Uint8Array;

    beforeEach(async () => {
      result = await subject.getContent(TEST_PATH);
    });

    it('returns buffer of fetchFileRaw', () => {
      expect(fetchFileRaw).toHaveBeenCalledWith(TEST_REF, TEST_PATH);
      expect(result).toEqual(TEST_FETCH_RESPONSE.buffer);
    });
  });
});
