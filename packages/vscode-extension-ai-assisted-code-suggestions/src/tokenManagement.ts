// WARNING: Please note that this file needs to stay somewhat in sync with
// the corresponding file in the `gitlab-vscode-extension` project.
// https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/src/completion/token_manager.ts
import { GitLabApiClient, PostRequest } from '@gitlab/vscode-mediator-commands';

export interface CompletionToken {
  access_token: string;
  /* expires in number of seconds since `created_at` */
  expires_in: number;
  /* unix timestamp of the datetime of token creation */
  created_at: number;
}

const tokenRequest: PostRequest<CompletionToken> = {
  type: 'rest',
  method: 'POST',
  path: 'code_suggestions/tokens',
};

export class CodeCompletionTokenManager {
  readonly #apiClient: GitLabApiClient;

  #currentToken: CompletionToken | undefined;

  constructor(apiClient: GitLabApiClient) {
    this.#apiClient = apiClient;
  }

  async getToken(): Promise<CompletionToken | undefined> {
    if (this.#currentToken) {
      const unixTimestampNow = Math.floor(new Date().getTime() / 1000);
      if (unixTimestampNow < this.#currentToken.created_at + this.#currentToken.expires_in) {
        return this.#currentToken;
      }
    }

    const token = await this.#apiClient.fetchFromApi(tokenRequest);
    this.#currentToken = token;

    return this.#currentToken;
  }
}
