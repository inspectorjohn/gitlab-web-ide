import * as vscode from 'vscode';
import { COMMAND_GET_CONFIG, IFullConfig } from '@gitlab/vscode-mediator-commands';

export const getConfig: () => Promise<IFullConfig> = async () =>
  vscode.commands.executeCommand(COMMAND_GET_CONFIG);
