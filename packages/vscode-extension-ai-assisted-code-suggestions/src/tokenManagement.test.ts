// WARNING: Please note that this file needs to stay somewhat in sync with
// the corresponding file in the `gitlab-vscode-extension` project.
// https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/src/completion/token_manager.test.ts
import type { GitLabApiClient } from '@gitlab/vscode-mediator-commands';
import { CodeCompletionTokenManager, CompletionToken } from './tokenManagement';

describe('CodeCompletionTokenManager', () => {
  describe('getToken', () => {
    let makeApiRequest: jest.Mock;

    const createApiClient = (response: CompletionToken): GitLabApiClient => {
      makeApiRequest = jest.fn(async <T>(): Promise<T> => response as unknown as T);

      return {
        fetchFromApi: makeApiRequest,
      };
    };

    it('should return a token', async () => {
      const tokenManager = new CodeCompletionTokenManager(
        createApiClient({
          access_token: '123',
          expires_in: 0,
          created_at: 0,
        }),
      );
      const token = await tokenManager.getToken();
      expect(token).not.toBe(undefined);
      expect(token?.access_token).toBe('123');
      expect(token?.expires_in).toBe(0);
      expect(token?.created_at).toBe(0);
    });

    it('should not call the service again if the token has not expired', async () => {
      const unixTimestampNow = Math.floor(new Date().getTime() / 1000);
      const mf = createApiClient({
        access_token: '123',
        expires_in: 3000,
        created_at: unixTimestampNow,
      });

      const tokenManager = new CodeCompletionTokenManager(mf);

      await tokenManager.getToken();
      await tokenManager.getToken();

      expect(makeApiRequest).toHaveBeenCalledTimes(1);
    });

    it('should call the service again if the token has expired', async () => {
      const unixTimestampExpired = Math.floor(new Date().getTime() / 1000) - 3001;

      const mf = createApiClient({
        access_token: '123',
        expires_in: 3000,
        created_at: unixTimestampExpired,
      });

      const tokenManager = new CodeCompletionTokenManager(mf);

      await tokenManager.getToken();
      await tokenManager.getToken();

      expect(makeApiRequest).toHaveBeenCalledTimes(2);
    });
  });
});
