// WARNING: Please note that this file needs to stay somewhat in sync with
// the corresponding file in the `gitlab-vscode-extension` project.
// https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/src/completion/gitlab_code_completion_provider.ts
import * as vscode from 'vscode';
import * as path from 'path';
import { GitLabApiClient } from '@gitlab/vscode-mediator-commands';
import { log } from '../log';
import {
  AI_ASSISTED_CODE_SUGGESTIONS_API_URL,
  AI_ASSISTED_CODE_SUGGESTIONS_LINES_BELOW_CURSOR,
} from '../constants';
import {
  getAiAssistedCodeSuggestionsConfiguration,
  AI_ASSISTED_CODE_SUGGESTIONS_MODE,
} from '../utils/extensionConfiguration';
import { CodeCompletionTokenManager } from '../tokenManagement';
import { CircuitBreaker } from './CircuitBreaker';
import { GitLabProject } from '../gitlab/gitlab_project';

export const CIRCUIT_BREAK_INTERVAL_MS = 10000;

export const MAX_ERRORS_BEFORE_CIRCUIT_BREAK = 4;

export class GitLabCodeCompletionProvider implements vscode.InlineCompletionItemProvider {
  #tokenManager: CodeCompletionTokenManager;

  #project: GitLabProject;

  #circuitBreaker = new CircuitBreaker(MAX_ERRORS_BEFORE_CIRCUIT_BREAK, CIRCUIT_BREAK_INTERVAL_MS);

  #server: string;

  #debouncedCall: ReturnType<typeof setTimeout> | undefined;

  #debounceTimeMs = 500;

  #noDebounce: boolean;

  constructor(
    tokenManager: CodeCompletionTokenManager,
    project: GitLabProject,
    noDebounce = false,
  ) {
    this.#tokenManager = tokenManager;
    this.#server = GitLabCodeCompletionProvider.#getServer();
    this.#debouncedCall = undefined;
    this.#noDebounce = noDebounce;
    this.#project = project;
  }

  static async registerGitLabCodeCompletion(
    context: vscode.ExtensionContext,
    client: GitLabApiClient,
    project: GitLabProject,
  ) {
    let subscription: vscode.Disposable | undefined;
    const dispose = () => subscription?.dispose();
    context.subscriptions.push({ dispose });

    const register = () => {
      const tokenManager = new CodeCompletionTokenManager(client);

      subscription = vscode.languages.registerInlineCompletionItemProvider(
        { pattern: '**' },
        new GitLabCodeCompletionProvider(tokenManager, project),
      );
    };
    if (getAiAssistedCodeSuggestionsConfiguration().enabled) {
      register();
    }

    vscode.workspace.onDidChangeConfiguration(e => {
      if (e.affectsConfiguration(AI_ASSISTED_CODE_SUGGESTIONS_MODE)) {
        if (!getAiAssistedCodeSuggestionsConfiguration().enabled) {
          dispose();
        } else {
          register();
        }
      }
    });
  }

  static #getServer(): string {
    const serverUrl = new URL(AI_ASSISTED_CODE_SUGGESTIONS_API_URL);
    return serverUrl.href.toString();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async fetchCompletions(params = {}): Promise<any> {
    const token = await this.#tokenManager.getToken();
    if (!token) {
      log.error('AI Assist: Could not fetch token');
      return [] as vscode.InlineCompletionItem[];
    }

    const requestOptions = {
      method: 'POST',
      headers: {
        'X-Gitlab-Authentication-Type': 'oidc',
        Authorization: `Bearer ${token.access_token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };

    const response = await fetch(this.#server, requestOptions);

    await this.#handleErrorReponse(response);

    const data = await response.json();
    return data;
  }

  async getPrompt(document: vscode.TextDocument, position: vscode.Position) {
    const contentAboveCursor = document.getText(
      new vscode.Range(0, 0, position.line, position.character),
    );

    const linesBelowCursor = AI_ASSISTED_CODE_SUGGESTIONS_LINES_BELOW_CURSOR;
    const contentBelowCursor = document.getText(
      new vscode.Range(position.line, position.character, position.line + linesBelowCursor, 0),
    );

    const projectPath = this.#project.namespaceWithPath || '';
    const projectId = this.#project.restId || -1;
    const fileName = path.basename(document.fileName) || '';

    const payload = {
      prompt_version: 1,
      project_path: projectPath,
      project_id: projectId,
      current_file: {
        file_name: fileName,
        content_above_cursor: contentAboveCursor,
        content_below_cursor: contentBelowCursor,
      },
    };

    return payload;
  }

  async getCompletions(document: vscode.TextDocument, position: vscode.Position) {
    if (this.#circuitBreaker.isBreaking()) {
      return [];
    }
    const prompt = await this.getPrompt(document, position);

    if (!prompt.current_file.content_above_cursor) {
      log.info('The prompt did not pass validation and was not sent to code suggestion');
      return [] as vscode.InlineCompletionItem[];
    }

    let data;

    try {
      data = await this.fetchCompletions(prompt);
      this.#circuitBreaker.success();
    } catch (e) {
      log.error(`Error obtaining suggestions:`, e as Error);
      this.#circuitBreaker.error();
      return [];
    }

    return (
      data.choices?.map(
        (choice: { text: string }) =>
          new vscode.InlineCompletionItem(
            choice.text as string,
            new vscode.Range(position, position),
          ),
      ) || []
    );
  }

  async provideInlineCompletionItems(
    document: vscode.TextDocument,
    position: vscode.Position,
    context: vscode.InlineCompletionContext,
  ): Promise<vscode.InlineCompletionItem[]> {
    clearTimeout(this.#debouncedCall);

    return new Promise(resolve => {
      //  In case of a hover, this will be triggered which is not desired as it calls for a new prediction
      if (context.triggerKind === vscode.InlineCompletionTriggerKind.Automatic) {
        if (this.#noDebounce) {
          resolve(this.getCompletions(document, position));
        } else {
          this.#debouncedCall = setTimeout(() => {
            resolve(this.getCompletions(document, position));
          }, this.#debounceTimeMs);
        }
      }
    });
  }

  async #handleErrorReponse(response: Response) {
    if (!response.ok) {
      const body = await response.text().catch(() => undefined);
      throw new Error(
        `Fetching code suggestions from ${response.url} failed for server ${
          this.#server
        }. Body: ${body}`,
      );
    }
  }
}
