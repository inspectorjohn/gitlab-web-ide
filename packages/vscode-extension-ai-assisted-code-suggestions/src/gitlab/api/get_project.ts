import { GraphQLRequest } from '@gitlab/vscode-mediator-commands';
import { gql } from 'graphql-request';
import { getRestIdFromGraphQLId } from '../get_rest_id_from_graphql_id';
import { GitLabProject } from '../gitlab_project';

interface GqlGroup {
  id: string;
}

export interface GqlProject {
  id: string;
  name: string;
  description: string;
  fullPath: string;
  webUrl: string;
  group: GqlGroup;
}

export interface GqlProjectResult {
  project?: GqlProject;
}

const fragmentProjectDetails = gql`
  fragment projectDetails on Project {
    id
    name
    description
    fullPath
    webUrl
    group {
      id
    }
  }
`;
const queryGetProject = gql`
  ${fragmentProjectDetails}
  query GetProject($namespaceWithPath: ID!) {
    project(fullPath: $namespaceWithPath) {
      ...projectDetails
    }
  }
`;

export const getProject: (
  namespaceWithPath: string,
) => GraphQLRequest<GqlProjectResult> = namespaceWithPath => ({
  type: 'graphql',
  query: queryGetProject,
  variables: { namespaceWithPath },
});

export const convertToGitLabProject: (gqlProject: GqlProject) => GitLabProject = gqlProject => ({
  ...gqlProject,
  gqlId: gqlProject.id,
  namespaceWithPath: gqlProject.fullPath,
  restId: getRestIdFromGraphQLId(gqlProject.id),
  groupRestId: getRestIdFromGraphQLId(gqlProject.group.id),
});
