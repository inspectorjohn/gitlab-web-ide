import * as vscode from 'vscode';
import { createStatusBarItem, CodeSuggestionStatusBarItem } from './CodeSuggestionStatusBarItem';
import { getAiAssistedCodeSuggestionsConfiguration } from '../utils/extensionConfiguration';

jest.mock('../utils/extensionConfiguration');

describe('StatusBar', () => {
  const mockStatusBarItem = {
    show: jest.fn(),
  } as unknown as vscode.StatusBarItem;

  describe('text and tooltip', () => {
    it('supports disabled state', () => {
      const sb = new CodeSuggestionStatusBarItem(mockStatusBarItem);
      sb.updateStatusBarText(false);

      expect(mockStatusBarItem.text).toEqual(`$(gitlab-code-suggestions-disabled)`);
      expect(mockStatusBarItem.tooltip).toEqual(`Code suggestions are disabled`);
    });

    it('supports enabled state', () => {
      const sb = new CodeSuggestionStatusBarItem(mockStatusBarItem);
      sb.updateStatusBarText(true);

      expect(mockStatusBarItem.text).toEqual(`$(gitlab-code-suggestions-enabled)`);
      expect(mockStatusBarItem.tooltip).toEqual(`Code suggestions are enabled`);
    });
  });

  describe('createStatusBarItem', () => {
    it('creates the item', () => {
      jest.mocked(vscode.window.createStatusBarItem).mockReturnValue(mockStatusBarItem);
      jest.mocked(getAiAssistedCodeSuggestionsConfiguration).mockReturnValue({ enabled: true });
      createStatusBarItem({
        subscriptions: [],
      } as unknown as vscode.ExtensionContext);

      expect(mockStatusBarItem.text).toEqual(`$(gitlab-code-suggestions-enabled)`);
      expect(mockStatusBarItem.tooltip).toEqual(`Code suggestions are enabled`);
      expect(vscode.workspace.onDidChangeConfiguration).toHaveBeenCalled();
    });
  });
});
