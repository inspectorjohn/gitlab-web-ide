import * as vscode from 'vscode';
import {
  AI_ASSISTED_CODE_SUGGESTIONS_MODE,
  getAiAssistedCodeSuggestionsConfiguration,
} from '../utils/extensionConfiguration';

export class CodeSuggestionStatusBarItem {
  statusBarItem: vscode.StatusBarItem;

  constructor(statusBarItem: vscode.StatusBarItem) {
    this.statusBarItem = statusBarItem;
  }

  updateStatusBarText(enabled: boolean) {
    let text = `$(gitlab-code-suggestions-disabled)`;
    let tooltip = 'Code suggestions are disabled';

    if (enabled) {
      text = `$(gitlab-code-suggestions-enabled)`;
      tooltip = 'Code suggestions are enabled';
    }

    this.statusBarItem.text = text;
    this.statusBarItem.tooltip = tooltip;
    this.statusBarItem.show();
  }
}

export const createStatusBarItem = (context: vscode.ExtensionContext) => {
  const statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
  context.subscriptions.push(statusBarItem);
  const statusBar = new CodeSuggestionStatusBarItem(statusBarItem);
  statusBar.updateStatusBarText(getAiAssistedCodeSuggestionsConfiguration().enabled);
  vscode.workspace.onDidChangeConfiguration((change: vscode.ConfigurationChangeEvent) => {
    if (change.affectsConfiguration(AI_ASSISTED_CODE_SUGGESTIONS_MODE)) {
      const { enabled } = getAiAssistedCodeSuggestionsConfiguration();
      statusBar.updateStatusBarText(enabled);
    }
  });
};
