import { VSBufferWrapper } from '@gitlab/vscode-mediator-commands';
import { BufferModule } from '../vscode';

export const createBufferWrapper =
  (bufferModule: BufferModule): VSBufferWrapper =>
  (actual: Uint8Array) =>
    bufferModule.VSBuffer.wrap(actual);
