import { joinPaths } from '@gitlab/utils-path';
import { GraphQLClient } from 'graphql-request';
import { createResponseError } from './createResponseError';
import { ApiRequest, GetRequest, GitLabApiClient, GraphQLRequest, PostRequest } from './types';

const withParams = (baseUrl: string, params: Record<string, string>) => {
  const paramEntries = Object.entries(params);

  if (!paramEntries.length) {
    return baseUrl;
  }

  const url = new URL(baseUrl);

  paramEntries.forEach(([key, value]) => {
    url.searchParams.append(key, value);
  });

  return url.toString();
};

export interface IDefaultGitLabClientConfig {
  baseUrl: string;
  authToken: string;
  httpHeaders?: Record<string, string>;
}

export class DefaultGitLabClient implements GitLabApiClient {
  readonly #baseUrl: string;

  readonly #httpHeaders: Record<string, string>;

  readonly #graphqlClient: GraphQLClient;

  constructor(config: IDefaultGitLabClientConfig) {
    this.#baseUrl = config.baseUrl;
    this.#httpHeaders = {
      ...(config.httpHeaders || {}),
      ...(config.authToken ? { 'PRIVATE-TOKEN': config.authToken } : {}),
    };

    const graphqlUrl = joinPaths(this.#baseUrl, 'api', 'graphql');
    this.#graphqlClient = new GraphQLClient(graphqlUrl, {
      headers: this.#httpHeaders,
    });
  }

  async fetchFromApi<T>(request: ApiRequest<T>): Promise<T> {
    if (request.type === 'rest' && request.method === 'GET') {
      return this.#makeGetRequest(request);
    }
    if (request.type === 'rest' && request.method === 'POST') {
      return this.#makePostRequest(request);
    }
    if (request.type === 'graphql') {
      return this.#makeGraphQLRequest(request);
    }
    throw new Error(`Unknown request type: ${(request as ApiRequest<T>).type}`);
  }

  async #makePostRequest<T>(request: PostRequest<T>): Promise<T> {
    const url = this.#appendPathToBaseApiUrl(request.path);
    return this.#fetchPostJson(url, request.body);
  }

  async #makeGetRequest<T>(request: GetRequest<T>): Promise<T> {
    const url = this.#appendPathToBaseApiUrl(request.path);
    return this.#fetchGetJson(url, request.searchParams);
  }

  #appendPathToBaseApiUrl(path: string) {
    return joinPaths(this.#baseUrl, 'api', 'v4', path);
  }

  async #fetchPostJson<TResponse, TBody>(url: string, body: TBody): Promise<TResponse> {
    const response = await fetch(url, {
      method: 'POST',
      body: body ? JSON.stringify(body) : undefined,
      headers: {
        ...this.#httpHeaders,
        'Content-Type': 'application/json',
      },
    });

    if (!response.ok) {
      throw await createResponseError(response);
    }

    return <Promise<TResponse>>response.json();
  }

  async #fetchGetJson<T>(url: string, params: Record<string, string> = {}): Promise<T> {
    const response = await this.#fetchGetResponse(url, params);

    return (await response.json()) as T;
  }

  async #fetchGetResponse(url: string, params: Record<string, string> = {}): Promise<Response> {
    const response = await fetch(withParams(url, params), {
      method: 'GET',
      headers: this.#httpHeaders,
    });

    if (!response.ok) {
      throw await createResponseError(response);
    }

    return response;
  }

  async #makeGraphQLRequest<T>(request: GraphQLRequest<T>): Promise<T> {
    return this.#graphqlClient.request(request.query, request.variables);
  }
}
