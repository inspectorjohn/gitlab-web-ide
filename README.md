# GitLab Web IDE

**STATUS:** In Development

A package for bootstrapping GitLab's context-aware Web IDE. Currently, this uses a browser build of [VSCode](https://github.com/microsoft/vscode).

## How to contribute?

Check out the [developer docs](./docs/dev/README.md).

## How to use the example?

You can run the example locally with `yarn start:example` or visit https://gitlab-org.gitlab.io/gitlab-web-ide/.

### For Client-only WebIDE

1. Fill out the startup configuration form, or accept the default values:

   | Field        | Value                   |
   | ------------ | ----------------------- |
   | Type         | `Client only (Default)` |
   | GitLab URL   | `https://gitlab.com`    |
   | Project Path | `gitlab-org/gitlab`     |
   | Ref          | `master`                |

2. Click **Start GitLab Web IDE**

### For Remote Development WebIDE

1. Fill out the startup configuration form

   | Field            | Value                |
   | ---------------- | -------------------- |
   | Type             | `Remote Development` |
   | Remote Authority | `localhost:9888`     |
   | Host Path        | `/home/user/project` |
   | Connection Token | `<token>`            |

- or add query parameters to URL: `?remoteHost=localhost:9888&hostPath=/home/user/project&tkn=password`

2. Click **Start GitLab Web IDE**

## References

Deployments:

- https://gitlab-org.gitlab.io/gitlab-web-ide/

Other projects:

- [GitLab VSCode Extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension)
- [Spike MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79096)
- [Spike playground project](https://gitlab.com/gitlab-org/frontend/playground/gitlab-vscode)
- [Temp playground project](https://gitlab.com/gitlab-org/frontend/playground/gitlab-vscode-prod)

## FAQ

### Does this infringe on user privacy by talking to Microsoft?

We are actively working on removing our runtime dependency on `vscode-cdn.net`
([see issue](https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/153)).
Rest assured that the dependency on this Third-Party is limited in nature.

It's not unique for GitLab to integrate with other services, but it's
imperative that these services comply with GDPR and privacy standards.
Thankfully (and not surprisingly), VSCode services promise compliance to
these standards ([see relevant docs](https://code.visualstudio.com/docs/supporting/FAQ#_gdpr-and-vs-code)).
